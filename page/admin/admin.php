<?php
namespace admin;
use Dunp\Auth\IPAuthProvider;
use Dunp\Plugin\Controller;
use Dunp\View\Twig\TwigView;

class admin_view extends TwigView
{

    public function display()
    {
        // TODO: Implement display() method.
        $this->setTheme("bootstrap");
        return true;
    }
}


class admin_controller extends Controller
{

    public function onStart()
    {
        $this->setAuthProvider(new IPAuthProvider());
        $this->setAuthorizationLevel(AUTHORIZATION_LEVEL_ADMIN);
    }

}

// The controller class is not required
