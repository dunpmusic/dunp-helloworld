<?php

class handle_view extends Dunp\Plugin\View
{
    public function display() {
        echo "Your query have numbers!";
        return false;
    }
}

class handle_controller extends \Dunp\Plugin\Controller
{
    public function onHandleUrl($params)
    {
        return preg_match("/([0-9]+)/", $params[0]);
    }

}