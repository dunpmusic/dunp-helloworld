<?php
\Dunp\Plugin\PluginRegister::addPlugin("dunp_helloworld");

class dunp_helloworld_plugin extends Dunp\Plugin\Plugin
{
    public function __construct($app, $path) {
        parent::__construct($app, $path, "HelloWorld", "dunpmusic", "", "Hola mundo!");
        $this->addCliCommand("helloworld");
        $this->addPage("index"); // Default index
        $this->addPage("handle"); // handle urls
        $this->addPage("admin/admin"); // sub-page

    }
}