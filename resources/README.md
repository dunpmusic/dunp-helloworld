## INFO

All files in this folder is visible for the browser.

You can access to the files using an absolute path (`/img/cat.png`) or using the plugin namespace path:
* `{{path}}/img/cat.png` : if you're using **Mustache**
* `{{ asset('/img/cat.png') }}` : if you're using **Twig**